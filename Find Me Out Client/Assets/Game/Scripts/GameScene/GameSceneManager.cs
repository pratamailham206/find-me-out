using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager instance;

    [Header("UI Elements")]
    public TextMeshProUGUI statusText;
    public TextMeshProUGUI caughtPlayersText;
    public TextMeshProUGUI timerText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }
    
    private void Start()
    {
        GameManager.instance.isLobbySession = false;
        GameManager.instance.isGameSession = true;
        
        foreach(Player _player in PlayerManager.instance.players.Values)
        {
            if(Client.instance.myId == _player.id) 
            {
                CameraFollow camFollow = Camera.main.GetComponent<CameraFollow>();
                camFollow.Follow(_player.transform);
            }

            if(PlayerManager.instance.seekerId == _player.id)
            {
                _player.SetRole(Roles.Seeker);
                PlayerManager.instance.InstantiateFieldOfView(_player.transform);
            }
            else
            {
                _player.SetRole(Roles.Hider);
            }
        }

        //If local player is seeker, set invisible to other players
        if(Client.instance.myId == PlayerManager.instance.seekerId)
        {
            statusText.text = "You're a Seeker";
            
            foreach(Player _player in PlayerManager.instance.players.Values)
            {
                if(_player.id != Client.instance.myId)
                {
                    _player.SetInvisible(true);
                }
            }
        }
        else
        {
            statusText.text = "You're a Hider";
        }

        UpdateGameSceneData();
    }

    public void SetTimer(int _time)
    {
        timerText.text = _time.ToString();
    }

    public void UpdateGameSceneData()
    {
        int _caughtPlayers = PlayerManager.instance.GetCaughtPlayers();
        int _totalHiders = PlayerManager.instance.GetTotalPlayers() - 1;

        caughtPlayersText.text = $"{_caughtPlayers}/{_totalHiders}";
    }

    public void SetStatusText(string _text)
    {
        statusText.text = _text;
    }
}
