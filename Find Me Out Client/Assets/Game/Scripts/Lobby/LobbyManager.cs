using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LobbyManager : MonoBehaviour
{
    public static LobbyManager instance;
    
    [Header("UI Elements")]
    public TextMeshProUGUI readyPlayersText;
    public TextMeshProUGUI totalPlayersText;
    public Button readyButton;
    public TextMesh usernameText;

    private TextMeshProUGUI readyButtonText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
        
        readyButtonText = readyButton.gameObject.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.instance.isLobbySession = true;
        GameManager.instance.isGameSession = false;

        if(!PlayerManager.instance.players.ContainsKey(Client.instance.myId))
        {
            ClientSend.SpawnPlayerRequest();
        }
        else
        {
            CameraFollow camFollow = Camera.main.GetComponent<CameraFollow>();
            camFollow.Follow(PlayerManager.instance.players[Client.instance.myId].transform);
        }
        UpdateLobbyData();

        readyButton.onClick.AddListener(ReadyButton);
    }

    public void SpawnPlayerInLobby(int _id, string _username, Vector3 _position, bool _isReady)
    { 
        Player _player;
        if(Client.instance.myId == _id)
        {
            _player = PlayerManager.instance.InstantiatePlayer(_position, true);

            //Set camera to follow local player
            CameraFollow camFollow = Camera.main.GetComponent<CameraFollow>();
            camFollow.Follow(_player.transform);
        }
        else
        {
            _player = PlayerManager.instance.InstantiatePlayer(_position);
        }

        Instantiate(PlayerManager.instance.spawnParticle, _position, Quaternion.identity);

        _player.Initialize(_id, _username, _isReady);
        _player.userText = Instantiate(usernameText, Vector3.zero, usernameText.transform.rotation);
        _player.userText.text = _player.username;

        PlayerManager.instance.AddPlayer(_id, _player);
        UpdateLobbyData();
        
        Debug.Log($"{_username} entered the lobby");
    }

    private void ReadyButton()
    {
        bool _value = !PlayerManager.instance.players[Client.instance.myId].isReady;
        
        ClientSend.PlayerIsReady(_value);
    }

    public void SetReadyButtonText(bool _value)
    {
        if(_value)
            readyButtonText.text = "Cancel";
        else
            readyButtonText.text = "Ready";
    }

    public void UpdateLobbyData()
    {
        readyPlayersText.text = PlayerManager.instance.GetReadyPlayers().ToString();
        totalPlayersText.text = PlayerManager.instance.GetTotalPlayers().ToString();
    }
}
