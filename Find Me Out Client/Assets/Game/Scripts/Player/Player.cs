using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int id;
    public string username;

    [Header("Lobby State")]
    public bool isReady;  
    
    [Header("In-Game State")]
    public Roles role = Roles.None;  
    public bool isCaught;

    public bool IsWalk { get; private set; }
    public TextMesh userText;

    private bool audioPlayed;

    private Animator _animator;
    private AudioSource _audioSource;
    private SkinnedMeshRenderer _renderer;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        _animator = GetComponentInChildren<Animator>();
        _renderer = GetComponentInChildren<SkinnedMeshRenderer>();
    }

    private void Update()
    {
        if(IsWalk && !audioPlayed)
        {
            _audioSource.Play();
            audioPlayed = true;
        }

        else if(!IsWalk && audioPlayed)
        {
            _audioSource.Stop();
            audioPlayed = false;
        }
    }

    private void LateUpdate()
    {
        if(userText != null)
        {
            userText.transform.position = new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.z);
        }
    }

    public void Initialize(int _id, string _username, bool _isReady)
    {
        id = _id;
        username = _username;
        isReady = _isReady;

        audioPlayed = false;
        IsWalk = false;
    }

    public void SetIsReady(bool _value)
    {
        isReady = _value;
    }

    public void SetRole(Roles _role)
    {
        role = _role;
    }

    public void SetIsCaught(bool _value)
    {
        isCaught = _value;
        Instantiate(PlayerManager.instance.caughtParticle, new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), Quaternion.identity);

        if (Client.instance.myId == PlayerManager.instance.seekerId)
        {
            SetInvisible(!_value);
        }
    }

    public void SetPosition(Vector3 _position)
    {
        if(transform.position == _position)
        {
            if(IsWalk) IsWalk = false;
        }
        else
        {
            if(!IsWalk) IsWalk = true;
            transform.position = _position;
        }
    }

    public void SetRotation(Quaternion _rotation)
    {
        transform.rotation = _rotation;
    }

    public void SetAnimation(string _animation, bool _value)
    {      
        _animator.SetBool(_animation, _value);
    }

    public void SetInvisible(bool _value) {
        float _alpha = 1;

        if (_value) _alpha = 0;
        else _alpha = 1;

        userText.color = new Color(userText.color.r, userText.color.g, userText.color.b, _alpha);
        _renderer.enabled = !_value;
    }
}
