using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;

    [Header("Game State")]
    public int seekerId;

    [Header("Player Prefabs")]
    public GameObject localPlayerPrefab;
    public GameObject playerPrefab;

    [Header("Other Prefabs")]
    public GameObject fieldOfViewPrefab;

    [Header("Particle Effect")]
    public GameObject spawnParticle;
    public GameObject caughtParticle;

    private GameObject fieldOfView;
    public Dictionary<int, Player> players;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

        players = new Dictionary<int, Player>();
    }

    public Player InstantiatePlayer(Vector3 _position, bool _localPlayer = false)
    {
        if(_localPlayer)
        {
            return Instantiate(localPlayerPrefab, _position, Quaternion.identity).GetComponent<Player>();
        }
        else
        {
            return Instantiate(playerPrefab, _position, Quaternion.identity).GetComponent<Player>();
        }
    }

    public void InstantiateFieldOfView(Transform _player)
    {
        fieldOfView = Instantiate(fieldOfViewPrefab, _player, false);
    }

    public void AddPlayer(int _id, Player _player)
    {
        _player.transform.parent = this.transform;
        players.Add(_id, _player);
        _player.userText.transform.parent = this.transform;
    }

    public void RemovePlayer(int _id)
    {
        Destroy(players[_id].userText);
        Destroy(players[_id].gameObject);
        players.Remove(_id);
    }

    public int GetCaughtPlayers()
    {
        int counter = 0;
        foreach(Player _player in players.Values)
        {
            if(_player.isCaught) counter++;
        }
        return counter;
    }

    public int GetTotalPlayers()
    {
        return players.Count;
    }

    public int GetReadyPlayers()
    {
        int counter = 0;
        foreach(Player _player in players.Values)
        {
            if(_player.isReady) counter++;
        }
        return counter;
    }

    public void VisibleAllPlayers()
    {
        foreach (Player player in players.Values)
        {            
            player.SetInvisible(false);
        }
    }

    public void ResetPlayersState()
    {
        foreach (Player _player in players.Values)
        {            
            _player.isReady = false;
            _player.isCaught = false;
        }
        Destroy(fieldOfView);
    }
}