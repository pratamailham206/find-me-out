using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using Firebase;
using Firebase.Auth;

public class AuthManager : MonoBehaviour
{
    public static AuthManager instance;

    private const float CONNECTION_TIMEOUT = 5f;
    private bool devMode = false;

    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser user;

    [Header("Login")]
    public TMP_InputField emailLoginField;
    public TMP_InputField passwordLoginField;
    public TextMeshProUGUI warningLoginText;
    public Button loginSubmitButton;
    public Button switchSignUpButton;

    [Header("Sign Up")]
    public TMP_InputField usernameField;
    public TMP_InputField emailSignUpField;
    public TMP_InputField passwordSignUpField;
    public TMP_InputField confirmationPasswordSignUpField;
    public TextMeshProUGUI warningSignUpText;
    public Button signUpSubmitButton;
    public Button switchLoginButton;

    [Header("Dashboard")]
    public TextMeshProUGUI welcomeText;
    public TextMeshProUGUI dashboardStatusText;
    public Button enterLobbyButton;
    public Button signOutButton;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if(dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void Start()
    {
        warningLoginText.text = "";
        warningSignUpText.text = "";
        dashboardStatusText.text = "";

        loginSubmitButton.onClick.AddListener(LoginButton);
        signUpSubmitButton.onClick.AddListener(SignUpButton);
        enterLobbyButton.onClick.AddListener(EnterLobbyButton);
        signOutButton.onClick.AddListener(SignOutButton);
        switchSignUpButton.onClick.AddListener(() => CanvasManager.instance.SwitchCanvas(CanvasType.SignUp));
        switchLoginButton.onClick.AddListener(() => CanvasManager.instance.SwitchCanvas(CanvasType.Login));
    }

    private void Update() 
    {
        if(Input.GetKeyDown(KeyCode.Tab) && !devMode)
        {
            welcomeText.text = "Welcome, developer";
            CanvasManager.instance.SwitchCanvas(CanvasType.Dashboard);
            
            devMode = true;
        }
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = FirebaseAuth.DefaultInstance;
    }

    private void LoginButton()
    {
        StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));
    }

    private void SignUpButton()
    {
        StartCoroutine(SignUp(usernameField.text, emailSignUpField.text, passwordSignUpField.text, confirmationPasswordSignUpField.text));
    }

    private void EnterLobbyButton()
    {
        StartCoroutine(EnterLobby());
    }

    private void SignOutButton()
    {
        auth.SignOut();
        CanvasManager.instance.SwitchCanvas(CanvasType.Login);
        
        if(devMode)
        {
            devMode = false;
        }
    }

    private IEnumerator SignUp(string _username, string _email, string _password, string _passwordConfirmation)
    {
        if(_username == "") {
            warningSignUpText.text = "Username must be filled";
            yield break;
        } 
        else if(_password != _passwordConfirmation)
        {
            warningSignUpText.text = "Password and Password confirmation is different";
            yield break;
        }

        var SignUpTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
        yield return new WaitUntil(predicate: () => SignUpTask.IsCompleted);

        if(SignUpTask.Exception != null) 
        {
            Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + SignUpTask.Exception);

            FirebaseException firebaseEx = SignUpTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
            string message = "Login Failed!";
            switch(errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WeakPassword:
                    message = "Weak Password";
                    break;
                case AuthError.EmailAlreadyInUse:
                    message = "Email Already In Use";
                    break;
            }
            warningSignUpText.text = message;
            yield break;
        }
        
        // Firebase user has been created.
        user = SignUpTask.Result;

        if(user != null)
        {
            UserProfile profile = new UserProfile { DisplayName = _username };

            var ProfileTask = user.UpdateUserProfileAsync(profile);
            yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

            if(ProfileTask.Exception != null) {
                Debug.LogError("UpdateUserProfileAsync encountered an error: " + ProfileTask.Exception);
            }
        }

        Debug.LogFormat("Firebase user created successfully: {0} ({1})", user.DisplayName, user.UserId);
        
        welcomeText.text = "Welcome, " + user.DisplayName;
        CanvasManager.instance.SwitchCanvas(CanvasType.Dashboard);
    }

    public IEnumerator Login(string _email, string _password)
    {
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null) {
            Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + LoginTask.Exception);

            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
            string message = "Login Failed!";
            switch(errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }
            warningLoginText.text = message;
            yield break;
        }

        user = LoginTask.Result;
        Debug.LogFormat("User signed in successfully: {0} ({1})", user.DisplayName, user.UserId);

        welcomeText.text = "Welcome, " + user.DisplayName;
        CanvasManager.instance.SwitchCanvas(CanvasType.Dashboard);
    }

    private IEnumerator EnterLobby()
    {
        if(devMode)
        {
            Client.instance.username = "developer";
        }
        else
        {
            Client.instance.username = user.DisplayName;
        }

        SetDashboardStatus("Connecting to server...");
        Client.instance.ConnectToServer();

        float counter = 0;
        while(counter < CONNECTION_TIMEOUT)
        {
            counter += Time.deltaTime; 
            if(Client.instance.isConnected)
            {
                SetDashboardStatus("Entering Lobby...");
                ClientSend.EnterLobbyRequest();

                yield break;
            }

            yield return null;
        }
        SetDashboardStatus("Connection timeout, can't connect to the server", 2f);
    }

    public void LoadLobby()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void SetDashboardStatus(string _message)
    {
        dashboardStatusText.text = _message;
    }
    
    public void SetDashboardStatus(string _message, float _timeout)
    {
        dashboardStatusText.text = _message;

        StartCoroutine(HideDashboardStatus(_timeout));
    }

    private IEnumerator HideDashboardStatus(float _timeout)
    {
        yield return new WaitForSeconds(_timeout);

        dashboardStatusText.text = "";
    }
}