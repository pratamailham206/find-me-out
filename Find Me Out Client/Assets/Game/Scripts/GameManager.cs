using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Roles 
{
    None,
    Seeker,
    Hider
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    [Header("Game State")]
    public bool isLobbySession;
    public bool isGameSession;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void StartGame(int _seekerId)
    {
        PlayerManager.instance.seekerId = _seekerId;
        SceneManager.LoadScene("GameScene");
    }

    public void EndGame(string _winner)
    {
        if(Client.instance.myId == PlayerManager.instance.seekerId)
        {
            PlayerManager.instance.VisibleAllPlayers();
        }
        GameSceneManager.instance.SetStatusText($"{_winner} is the winner");

        StartCoroutine(EndGameAfterTimeout(5f));
    }

    private IEnumerator EndGameAfterTimeout(float _timeout)
    {
        yield return new WaitForSeconds(_timeout);

        PlayerManager.instance.ResetPlayersState();
        SceneManager.LoadScene("Lobby");
    }
}
