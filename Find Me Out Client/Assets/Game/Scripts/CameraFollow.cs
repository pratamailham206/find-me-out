using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public float camHeight = 6.2f;
    public float camDist = 5.8f;
    
    public Transform playerTransform;
    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 1f;
    public bool LookAtPlayer = true;

	private void Update () {
        if(playerTransform == null) return;

        Vector3 newPos = playerTransform.position + _cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        if (LookAtPlayer)
            transform.LookAt(playerTransform);
	}

    public void Follow(Transform _playerTransform) {
        playerTransform = _playerTransform;

        transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y + camHeight, playerTransform.position.z - camDist);
        _cameraOffset = transform.position - playerTransform.position;	
	}
}
