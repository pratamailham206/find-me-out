using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager instance;

    public CanvasType defaultCanvas;
    private CanvasController lastActiveCanvas;

    private List<CanvasController> canvasControllersList;

    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {
        canvasControllersList = new List<CanvasController>();

        CanvasController[] _canvasControllers = GetComponentsInChildren<CanvasController>();
        foreach(CanvasController _canvas in _canvasControllers)
        {
            if(_canvas.type != defaultCanvas) 
            {
                _canvas.EnabledCanvas(false);
            } 
            else 
            {
                _canvas.EnabledCanvas(true);
                lastActiveCanvas = _canvas;
            }

            canvasControllersList.Add(_canvas);
        }
    }

    public void SwitchCanvas(CanvasType _canvasType)
    {
        CanvasController desiredCanvas = FindCanvasController(_canvasType);
        
        if(desiredCanvas != null) 
        {
            lastActiveCanvas.EnabledCanvas(false);
            desiredCanvas.EnabledCanvas(true);

            lastActiveCanvas = desiredCanvas;
        }
        else 
        {
            Debug.Log("Cannot find desired canvas");
        }
    }

    public CanvasController FindCanvasController(CanvasType _canvasType)
    {
        foreach(CanvasController _canvas in canvasControllersList)
        {
            if(_canvas.type == _canvasType) return _canvas;
        }

        return null;
    }
}