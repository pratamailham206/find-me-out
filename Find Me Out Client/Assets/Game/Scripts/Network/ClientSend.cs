﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }

    #region Packets
    public static void TCPTestReceived()
    {
        using (Packet _packet = new Packet((int)ClientPackets.TCPTestReceived))
        {
            _packet.Write(Client.instance.myId);
            _packet.Write(Client.instance.username);
            
            SendTCPData(_packet);
        }
    }

    public static void EnterLobbyRequest()
    {
        using (Packet _packet = new Packet((int)ClientPackets.enterLobbyRequest))
        {
            SendTCPData(_packet);
        }
    }

    public static void SpawnPlayerRequest()
    {
        using (Packet _packet = new Packet((int)ClientPackets.spawnPlayerRequest))
        {
            SendTCPData(_packet);
        }
    }

    public static void PlayerIsReady(bool _value)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerIsReady))
        {
            _packet.Write(_value);

            SendTCPData(_packet);
        }
    }

    public static void PlayerInput(float _inputX, float _inputZ)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerInput))
        {
            _packet.Write(_inputX);
            _packet.Write(_inputZ);

            SendUDPData(_packet);
        }
    }
    #endregion
}
