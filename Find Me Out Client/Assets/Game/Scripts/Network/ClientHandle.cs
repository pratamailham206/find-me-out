﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    public static void TCPTestHandler(Packet _packet)
    {
        int _myId = _packet.ReadInt(); 
        string _message = _packet.ReadString();

        Client.instance.myId = _myId;
        Debug.Log(_message);       
        
        ClientSend.TCPTestReceived();
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void UDPTestHandler(Packet _packet)
    {
        string _message = _packet.ReadString();
        Debug.Log(_message);

        Client.instance.isConnected = true;
    }

    public static void EnterLobbyFeedback(Packet _packet)
    {
        bool _value = _packet.ReadBool();

        if(_value)
        {
            AuthManager.instance.LoadLobby();        
        } 
        else
        {
            string _message = _packet.ReadString();
            
            AuthManager.instance.SetDashboardStatus(_message);
        }
    }

    public static void SpawnPlayerInLobby(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        bool _isReady = _packet.ReadBool();

        LobbyManager.instance.SpawnPlayerInLobby(_id, _username, _position, _isReady);
    }

    public static void PlayerIsReady(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _value = _packet.ReadBool();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.players[_id].SetIsReady(_value);
            if(_id == Client.instance.myId)
            {
                LobbyManager.instance.SetReadyButtonText(_value);
            }
            LobbyManager.instance.UpdateLobbyData();
        }
    }

    public static void StartGame(Packet _packet)
    {
        int _seekerId = _packet.ReadInt();

        GameManager.instance.StartGame(_seekerId);
    }

    public static void PlayerPosition(Packet _packet)
    {   
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.players[_id].SetPosition(_position);
        }
    }

    public static void PlayerRotation(Packet _packet)
    {   
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.players[_id].SetRotation(_rotation);
        }
    }

    public static void PlayerAnimation(Packet _packet)
    {   
        int _id = _packet.ReadInt();
        string _animation = _packet.ReadString();
        bool _value = _packet.ReadBool();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.players[_id].SetAnimation(_animation, _value);
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {   
        int _id = _packet.ReadInt();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.RemovePlayer(_id);

            if(GameManager.instance.isLobbySession)
            {
                LobbyManager.instance.UpdateLobbyData();
            }
            else if(GameManager.instance.isGameSession)
            {
                GameSceneManager.instance.UpdateGameSceneData();
            }
            Debug.Log($"Player {_id} is disconnected");
        }
    }

    public static void PlayerIsCaught(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _value = _packet.ReadBool();

        if(PlayerManager.instance.players.ContainsKey(_id))
        {
            PlayerManager.instance.players[_id].SetIsCaught(_value);
            GameSceneManager.instance.UpdateGameSceneData();
        }
    }

    public static void TimerData(Packet _packet)
    {
        int _timer = _packet.ReadInt();
        GameSceneManager.instance.SetTimer(_timer);
    }
    
    public static void EndGame(Packet _packet)
    {
        string _winner = _packet.ReadString();

        GameManager.instance.EndGame(_winner);
    }
}
