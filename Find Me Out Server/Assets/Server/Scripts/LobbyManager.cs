using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyManager : MonoBehaviour
{
    public static LobbyManager instance;

    [Header("Spawn Settings")]
    public Transform spawnPos;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {   
        NetworkManager.instance.isGameSession = false;
        NetworkManager.instance.isLobbySession = true;
    }

    public void SpawnPlayerInLobby(int _id)
    {
        Player _player = PlayerManager.instance.InstantiatePlayer(spawnPos.position);
        _player.Initialize(_id);

        Server.clients[_id].player = _player;

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.id != _id)
                {
                    ServerSend.SpawnPlayerInLobby(_id, _client.player);
                }
            }
        }

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                ServerSend.SpawnPlayerInLobby(_client.id, _player);
            }
        }
    }
}
