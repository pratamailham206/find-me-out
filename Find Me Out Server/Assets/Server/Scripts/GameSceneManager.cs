using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager instance;

    public const float ONE_SECOND = 1.0f;
    public const int COUNTDOWN_TIME = 60;

    private float timer;
    public int countdownTime;
    public bool timerStarted;

    [Header("Seeker Spawn list")]
    public Transform[] SSpawnPos;

    [Header("Hider Spawn list")]
    public Transform[] HSpawnPos;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {   
        NetworkManager.instance.isGameSession = true;
        NetworkManager.instance.isLobbySession = false;

        //Instantiate fov as seeker child, world position false
        Player _seeker = PlayerManager.instance.GetSeeker();
        PlayerManager.instance.InstantiateFieldOfView(_seeker.transform);
        
        SetSpawnPos();

        countdownTime = COUNTDOWN_TIME;
        timer = ONE_SECOND;

        timerStarted = true;
    }

    private void Update() 
    {
        Timer();
    }

    public void SetSpawnPos()
    {
        foreach(Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.player.Role == Roles.Hider)
                {
                    int _randomPos = Random.Range(0, HSpawnPos.Length - 1);
                    _client.player.transform.position = HSpawnPos[_randomPos].position;
                }
                else
                {
                    int _randomPos = Random.Range(0, SSpawnPos.Length - 1);
                    _client.player.transform.position = SSpawnPos[_randomPos].position;
                }
            }                
        }
    }

    private void Timer()
    {
        if(timerStarted)
        {
            if (countdownTime <= 0) return;

            if (timer >= 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                countdownTime--;
                ServerSend.SendTimerData(countdownTime);

                if (countdownTime <= 0)
                {
                    NetworkManager.instance.SetWinner("Hider");
                }

                timer = ONE_SECOND;
            }
        }        
    }
}
