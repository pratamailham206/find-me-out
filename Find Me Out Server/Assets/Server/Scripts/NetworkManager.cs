using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Roles 
{
    None,
    Seeker,
    Hider
}

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    [Header("Default Server Settings")]
    public string ipAddress = "127.0.0.1";
    public int port = 26950;

    [Header("Game State")]
    public bool isLobbySession;
    public bool isGameSession;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else 
        {
            Debug.Log("Instance is already exist, destroying object");
            Destroy(this);
        }
    }

    private void Start()
    {
        //Limit the application to reduce memory usage
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        Server.Start();
    }

    private void OnApplicationQuit()
    {
        Server.CloseSocket();
    }

    public void StartGame()
    {
        Debug.Log("Starting game...");
        PlayerManager.instance.SetSeeker();
        ServerSend.StartGame();
        SceneManager.LoadScene("GameScene");
    }

    public void SetWinner(string _winner)
    {
        GameSceneManager.instance.timerStarted = false;
        ServerSend.EndGame(_winner);
        Debug.Log($"{_winner} is the winner");
        EndGame(5);
    }

    public void EndGame(float _timer)
    {
        StartCoroutine(EndGameAfterTimeout(_timer));
    }

    private IEnumerator EndGameAfterTimeout(float _timeout)
    {
        yield return new WaitForSeconds(_timeout);
        Debug.Log("Returning back to lobby...");
        SceneManager.LoadScene("Lobby");

        Debug.Log("Ending game...");
        Debug.Log("Resetting players state...");
        PlayerManager.instance.ResetPlayersState();
    }
}
