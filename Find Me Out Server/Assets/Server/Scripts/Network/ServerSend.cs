﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ServerSend
{
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= PlayerManager.instance.maxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= PlayerManager.instance.maxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

        private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= PlayerManager.instance.maxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= PlayerManager.instance.maxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }

    #region Packets
    public static void SendTCPTest(int _toClient, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.TCPTestServer))
        {
            _packet.Write(_toClient);
            _packet.Write(_msg);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void SendUDPTest(int _toClient, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.UDPTestServer))
        {
            _packet.Write(_msg);

            SendUDPData(_toClient, _packet);
        }
    }

    public static void EnterLobbyFeedback(int _id, bool _value)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enterLobbyFeedback))
        {
            _packet.Write(_value);

            SendTCPData(_id, _packet);
        }
    }

    public static void EnterLobbyFeedback(int _id, bool _value, string _message)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enterLobbyFeedback))
        {
            _packet.Write(_value);
            _packet.Write(_message);

            SendTCPData(_id, _packet);
        }
    }

    public static void SpawnPlayerInLobby(int _id, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.isReady);

            SendTCPData(_id, _packet);
        }
    }

    public static void SendPlayerIsReady(int _id, bool _value)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerIsReady))
        {
            _packet.Write(_id);
            _packet.Write(_value);

            SendTCPDataToAll(_packet);
        }
    }

    public static void StartGame()
    {
        using (Packet _packet = new Packet((int)ServerPackets.startGame))
        {
            _packet.Write(PlayerManager.instance.seekerId);

            SendTCPDataToAll(_packet);
        }
    }

    public static void SendPlayerPosition(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);

            SendUDPDataToAll(_packet);
        }
    }

    public static void SendPlayerRotation(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }

    public static void SendPlayerAnimation(int _id, string _animation, bool _value)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerAnimation))
        {
            _packet.Write(_id);
            _packet.Write(_animation);
            _packet.Write(_value);

            SendUDPDataToAll(_packet);
        }
    }

    public static void SendPlayerIsCaught(int _id, bool _value)
    {
        using (Packet _packet = new Packet ((int)ServerPackets.playerIsCaught))
        {
            _packet.Write(_id);
            _packet.Write(_value);

            SendTCPDataToAll(_packet);
        }
    }

    public static void SendTimerData(int _time)
    {
        using (Packet _packet = new Packet((int)ServerPackets.timerData))
        {
            _packet.Write(_time);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerDisconnected(int _id)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_id);

            SendTCPDataToAll(_packet);
        }
    }

    public static void EndGame(string _winner)
    {
        using (Packet _packet = new Packet((int)ServerPackets.endGame))
        {
            _packet.Write(_winner);

            SendTCPDataToAll(_packet);
        }
    }
    #endregion
}
