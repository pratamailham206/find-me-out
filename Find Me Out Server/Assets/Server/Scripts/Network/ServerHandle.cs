﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void TCPTestReceived(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();        
        string _username = _packet.ReadString();

        Client _client = Server.clients[_fromClient];
        _client.username = _username;

        Debug.Log($"{_client.tcp.socket.Client.RemoteEndPoint} connected successfully, welcome {_client.username}");
        if (_fromClient != _clientIdCheck)
        {
            Debug.Log($"Player ID: {_fromClient} has assumed the wrong client ID ({_clientIdCheck})!");
        }
    }

    public static void EnterLobbyRequest(int _fromClient, Packet _packet)
    {
        if(NetworkManager.instance.isLobbySession)
        {   
            ServerSend.EnterLobbyFeedback(_fromClient, true);
        } 
        else if(NetworkManager.instance.isGameSession)
        {
            ServerSend.EnterLobbyFeedback(_fromClient, false, "Failed entering lobby, game is already started");
        }
    }

    public static void SpawnPlayerRequest(int _fromClient, Packet _packet)
    {
        LobbyManager.instance.SpawnPlayerInLobby(_fromClient);
    }

    public static void PlayerIsReady(int _fromClient, Packet _packet)
    {
        bool _value = _packet.ReadBool();

        Server.clients[_fromClient].player.SetIsReady(_value);

        if(PlayerManager.instance.AllPlayerIsReady())
        {
            Debug.Log("All players is ready, starting game...");
            NetworkManager.instance.StartGame();
        }
    }

    public static void PlayerInput(int _fromClient, Packet _packet)
    {           
        float _inputX = _packet.ReadFloat();
        float _inputZ = _packet.ReadFloat();
        
        Server.clients[_fromClient].player.SetInput(_inputX, _inputZ);

    }
}