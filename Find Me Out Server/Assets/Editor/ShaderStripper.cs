#define SKIP_SHADER_COMPILATION

#if UNITY_SERVER_BUILD
using System.Collections.Generic;
using UnityEditor.Build;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Rendering;

public class ShaderStripper : IPreprocessShaders
{
	public int callbackOrder { get { return 0; } }

	public void OnProcessShader( Shader shader, ShaderSnippetData snippet, IList<ShaderCompilerData> data )
	{
		string shaderName = shader.name;

		for( int i = data.Count - 1; i >= 0; --i )
		{
			data.RemoveAt( i );
		}

#if SKIP_SHADER_COMPILATION
		for( int i = data.Count - 1; i >= 0; --i )
			data.Clear();
#endif
	}
}
#endif