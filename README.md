<h1 align="center">Find Me Out</h1>

[![Made with Unity](https://img.shields.io/badge/Made%20with-Unity-57b9d3.svg?style=for-the-badge&logo=unity)](https://unity3d.com)

Find me out is a casual online game like hide and seek. when you play this game, all player will be divide into two roles (hider and seeker) which are chosen randomly. when player as a hider, player must catch the hider before time out. player cant see where hider is. but there are such item that can give the seeker advantage to catch hider. as a seeker, to win this game player must catch all hider before timeout, and for the hider, player must alive untuk time run out. hider who have been caught, other hider can release caught hider to help win the game.

<img src="Documents/Images/2.PNG"  width="49.5%">

<img src="Documents/gif/tgs1.gif" alt="hider gif"  width="49.5%" />

#### Content

- Play as Hider : Avoid seeker from arena adn stay alive until time runs out. 
- Play as Seeker : Catch all hider before time runs out, use the item in arena for easy capture.
- Customize your character with all available skins in the game.
- Vote your favorite map to play in next match.

#### Genre

Casual, Running, Maze

#### Platform

Windows

# Play The Game

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See networking tutorial from [here](https://www.youtube.com/playlist?list=PLXkn83W0QkfnqsK8I0RAz5AbUxfg3bOQ5) when you need better information about networking. Clone project. After cloning or downloading the repository, open the root project folder in unity and press the Play button. or you can just download build version of this game [here](https://ilhampratama.itch.io/find-me-out)

**See Find Me Out gameplay through this gif.**

<img src="Documents/gif/tgs2.gif" alt="hider gif"  width="69.5%" />

#### Prerequisites

-  Unity ver. 2020.2 or higher

#### Download release version

Want to try the game out? Head to the release page and [download](https://ilhampratama.itch.io/find-me-out) the latest version.

#### See high concept and design

* Slide presentation [here](https://docs.google.com/presentation/d/1iKG1R4ODLgdfETCUvkQC1HMsZjzAIpZa15-YXZ4JAZA/edit#slide=id.p)
* High concept document [here](https://gitlab.com/pratamailham206/find-me-out/-/blob/master/Documents/4210181020_4210181021_GDD_Find_Me_Out.docx)


# Authors

* Ilham Pratama - [Gitlab](https://gitlab.com/pratamailham206)
* Aditya Nur Juang R - [Gitlab](https://gitlab.com/adityarahmanda)

# Network-Documentation

this project is created using Unity with native C# TCP UDP protocol, integrated with firebase to store account and data. Architecture-Connection Design of find me out is created using a system of packets sent using TCP and UDP protocols, each packet has its own identifier in the form of int which is initialized in the enum, then each type of packet / packet identifier will be initialized in the form of a dictionary. so that before the packet is sent from the client or server side, the packet needs to be identified. then when the packet is received by the data handle. packets will be identified and handled by the function initialized in the dictionary in each Server or Cient side.

### Feature-Documentation

Description of all feature in Find Me Out game

##### 1. Account-Authentication

In this project we use firebase to store account and user authentication. Before playing this game, new player need to register in available form. User data like username, email, and password will be send from client to server, and then we validate with firebase api. If data is valid, client will receive response and message from server, and then will be redirected to main menu. Then sever will validate the data and give response to client.

##### 2. Lobby System

When player start entering lobby, client will send spawn request and server will instance a player and send lobby data like total player and player who’s ready .
When other player is entering lobby, server will broadcast spawn result and player data, so other player can have same lobby data.

##### 3. Wardrobe System

In lobby, player can also change character’s skin. There are 8 skin available that can be use in game. Where player change their skin, client will send player Id and material Id, and then server will broadcast material Id to all client, so all client will process material Id and set base material to set material which has same id.

##### 4. Vote system

In vote system, player sent map Id who’s voted. And then server will save number of voted map. When match start, the most voted map will be selected.

##### 5. Game start

When the player ready, server will check if player ready and total player number. If it has same number. Server will broadcast data to all client, so all player can start the game.

##### 6. Timer

When game start, server will create a timer for time of play in this match. For each second, server will send time data to all player. And check if when time has run out.

##### 7 .Collision

If there’s any collision between hider and seeker, server will broadcast data for player id who’s caught, make that player can’t move. And then client will process id like add animation, and update game data.

##### 8. Item spawn

When match is running Server will create item spawn mechanic, and then server will send spawn pos to all client.

##### 9. Mud system

In server side, when player collide with mud, decrease speed of player. And if player is out from mud, server will send  player Id data to all client, and then client will process data to add footprint in out character

##### 10. Win Announce

If countdown time has run out, server will set winner to all hider, and send data to all client. Client will process data to show winner. Also server will check if all hider is caught. Server will set winner to seeker, and send data to all client.

### Network Flow Diagram Design

<img src="Documents/Images/arch.png">

#### Flow Design

1. Server started to run and initialize firebase service. then client connect to tcp connection in server if success, client save endpoint of server and start make connection to UDP server

2. Client send authentication request like Login or Register to server. send Username and Password using TCP connection, then server will make request to firebase and validate user. if user is valid, server give response to client and if response is valid, client will be able to continue into game and redirected to Main Menu.

3. When player clicked play, client will send lobbyRequest to server. then server will check if match is not started. server will give response data like total player in Lobby and player ready. Client, again start to send spawnRequest to server. server will create player data(id, spawnpos) and give response player data and otherplayerdata in lobby so all player can shown in each player.

4. in lobby, if player had an input, client will send input to server, and server will continue to overwrite the input and process data so character will be able to move and server give response in playedata (id, Vector position, animation, quarternion) and broadcast data to all client. in client side, cient will write data and update character position according to id data received from server.

5. lobby, player can also change skin of character they had. client will send materialId according to skin they select. and then server will broadcast materialId selected to all client. in each client will write and update data so skin in character can be changed.

6. Game will started when all player is ready. when player clicked ready. client will send bool data and server will checked if all player ready is same with total player in lobby. server will start game and send broadcast to all client to start the match.

7. When match is started, server will random seeker role and give result to selected client. that client will have role as a seeker, and the other client is hider.

8. server will run timer for match duration. each second server will broadcast remaining time to client. and if time is run out. server will set winner to all hider and send data to all client. client will show winner in each game. 

9. when hider is caught by seeker, server will send data to all client and make caught player cant move, when each player caught by seeker. server will check remaining hider left. if there's no hider left, server will set winner to seeker and send data to all client. client will show winner to seeker in each player.

# Screenshot

<img src="Documents/Images/1.PNG"  width="49.5%">
<img src="Documents/Images/6.PNG"  width="49.5%">
<img src="Documents/Images/5.PNG"  width="49.5%">
<img src="Documents/Images/7.PNG"  width="49.5%">
<img src="Documents/Images/8.PNG"  width="49.5%">
<img src="Documents/Images/9.PNG"  width="49.5%">
<img src="Documents/Images/11.PNG"  width="49.5%">
<img src="Documents/Images/8.PNG"  width="49.5%">
<img src="Documents/Images/10.PNG"  width="49.5%">
<img src="Documents/Images/12.PNG"  width="49.5%">

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
